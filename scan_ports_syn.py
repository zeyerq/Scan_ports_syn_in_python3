#!/usr/bin/env python3.11

import sys;
try:
    import logging;
    logging.getLogger("scapy.runtime").setLevel(logging.ERROR);
    from scapy.all import *;
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages scapy");
try:
    import argparse;
    from argparse import RawTextHelpFormatter;
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse");

parser = argparse.ArgumentParser(description="Port scan using syn flag", 
                                 formatter_class=RawTextHelpFormatter, 
                                 epilog="Examples:\n"
                                        "./scan_ports_syn.py --ip 44.228.249.3 --port 80,443,8080");
parser.add_argument("--ip", "-i", help="ip address, ex: 44.228.249.3", required=True);
parser.add_argument("--port", "-p", help="port list, ex: 80,443,8080", required=True);
args = parser.parse_args();


def scan_port(host, port):
    src_port = RandShort()
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"};
    syn_packet = IP(dst=host)/TCP(sport=src_port, dport=port, flags='S')

    response = sr1(syn_packet, timeout=1, verbose=0)

    if response is not None:
        if response.haslayer(TCP) and response.getlayer(TCP).flags == 'SA':
            print(f"The port {port} is open")
        elif response.haslayer(TCP) and response.getlayer(TCP).flags == 'RA':
            print(f"The port {port} is closed")
        else:
            print(f"Unable to determine state of port {port}")
    else:
        print(f"The port {port} is filtered or there was no response")

host = sys.argv[2];
#ports = [80, 443, 8080]

port_str = sys.argv[4]
port_list = port_str.split(',')
port_list = [port.strip() for port in port_list]
port = [int(port) for port in port_list]

for ports in port:
    scan_port(host, ports)
